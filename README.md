
## LOST IN TRANSLATION

### What is this?

This is a website that mainly does one thing. It translates text to sign language.
It also keeps track of the last 10 translations done. To do so, it needs
your name or alias, and it will make a user connected to this name.

### Known problems

On creating a new user: the application does not
work as it is supposed to. The translation functionality is unresponsive
and the profile page is inaccessible. If this happens, clear sessionStorage
and login again. Then it will work. This was discovered too late in development to fix before submitting.

### Who worked on this?

Petter Fiskvik and Marius Olafsen worked on this project.

### Deployment

The solution is deployed to heroku at: 
https://mighty-river-36697.herokuapp.com/translation




