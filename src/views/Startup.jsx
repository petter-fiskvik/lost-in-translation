import StartupForm from '../components/Startup/StartupForm';
import '../css/startup.css'

const Startup = () => {
    return (
        <section id="startup-body">
            <StartupForm />
        </section>
    )
}
export default Startup