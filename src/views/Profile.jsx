import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import ProfileActions from "../components/Profile/ProfileActions"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"
/* import { useEffect } from "react"
import { findUserById } from "../api/user"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { storageSave } from "../storage/storage" */
import '../css/profile.css'

const Profile = () => {

    const { user, /* setUser */ } = useUser()

    /* useEffect(() => {
        const findUser = async () => {
            const [error, latestUser] = await findUserById(user.id)
            if (error === null) {
                storageSave(STORAGE_KEY_USER, latestUser)
                setUser(latestUser)
            }
        }

        findUser()

    }, [setUser, user.id]) */

    return (
        <div id="profile-body">
            <h1>Welcome back {user.username}</h1>
            <ProfileTranslationHistory translations={user.translations}/>
            <ProfileActions />
        </div>
    )
}
export default withAuth(Profile)