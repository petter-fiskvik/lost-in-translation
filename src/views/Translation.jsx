import { useState } from "react"
import { addTranslation } from "../api/translation"
import TranslationForm from "../components/Translation/TranslationForm"
import TranslationResult from "../components/Translation/TranslationResult"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../storage/storage"
import '../css/translation.css'

const Translation = () => {
    const [toTranslate, setToTranslate] = useState()
    const {user, setUser} = useUser()

    const handleTranslateClicked = async translationText => {
        const [error, updatedUser] = await addTranslation(user, translationText)

        if (error !== null) {
            console.log(error)
            return
        }

        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
        setToTranslate(translationText)
    }

    return (
        <section id="translation-body">
            <h1 id="translation-header">Translation</h1>
            <section id="translation-form-section">
                <TranslationForm onTranslate={handleTranslateClicked} />
            </section>
            { toTranslate &&
            <section>
                <TranslationResult translationText={toTranslate} />
            </section>
            }
        </section>
    )
}
export default withAuth(Translation)