import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory = ({ translations }) => {

    // Copy the array of translations
    let translationsToMap = [...translations]
    // Because we don't want to reverse the original
    translationsToMap.reverse()
    
    // Limit to ten items in the translation history
    const translationList = translationsToMap.slice(0, 10).map(
        (translation, index) => <ProfileTranslationHistoryItem key={index} translation={translation} />)

    return (
        <section>
            <h3>Your last 10 searches</h3>
            <ul>
                {translationList}
            </ul>
        </section>
    )
}
export default ProfileTranslationHistory