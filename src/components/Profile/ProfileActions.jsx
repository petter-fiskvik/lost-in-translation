import { translationClearHistory } from "../../api/translation"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../storage/storage"

const ProfileActions = ({logout}) => {

    const { user, setUser } = useUser()

    const handleLogoutClick = () => {
        if (window.confirm('Log out?')) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    const handleClearHistoryClick = async() => {
        if (!window.confirm('Clear history?\nThis cannot be undone')) {
            return
        }

        const [clearError] = await translationClearHistory(user.id)
        
        if (clearError !== null) {
            // TODO
            return
        }
        const updatedUser = {
            ...user,
            translations: []
        }
        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }

    return (
        <section id="profile-actions-section">
            <button id="profile-button" onClick={handleClearHistoryClick} >Clear</button>
            <button id="profile-button" onClick={handleLogoutClick}>Logout</button>
        </section>
    )
}
export default ProfileActions