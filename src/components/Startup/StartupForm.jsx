import { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import {loginUser} from '../../api/user'
import { storageSave } from '../../storage/storage'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'

const usernameConfig = {
    required: true
}

const StartupForm = () => {
    const { register, handleSubmit, formState: { errors } } = useForm()
    const {user, setUser} = useUser()
    const navigate = useNavigate()

    // Local state
    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)

    // Side effects
    useEffect(() => {
        if (user !== null) {
            navigate('translation')
        }
        console.log("User has changed", user)
    }, [user, navigate])

    // Event handlers
    const onSubmit = async ({username}) => {
        setLoading(true)
        const [error, userResponse] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false)
    }

    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }

        if (errors.username.type === 'required') {
            return <span>Username is required</span>
        }
    })()

    return (
        <>
            <h2>Let's get started</h2>
            <form id="startup-form" onSubmit={handleSubmit(onSubmit)}>
                <fieldset>
                    <label htmlFor="username">Username: </label>
                    <input
                        id="startup-input"
                        type="text"
                        placeholder="Username"
                        {...register("username", usernameConfig)} />
                        <button id="startup-submit" type="submit" disabled={loading}>Continue</button>
                    {errorMessage}
                </fieldset>
                
                {loading && <p>Logging in...</p>}
                {apiError && <p>{apiError}</p>}
            </form>
        </>
    )
}
export default StartupForm