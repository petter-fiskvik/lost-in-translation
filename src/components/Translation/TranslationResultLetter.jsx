const TranslationResultLetter = ({letter}) => {
    const path = "signs/"
    const extension = ".png"

    return (
        <li>
            <img alt={letter} src={path + letter + extension} class="translation-list-item" />
        </li>
    )
}
export default TranslationResultLetter