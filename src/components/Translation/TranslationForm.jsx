import { useForm } from "react-hook-form"

const TranslationForm = ({ onTranslate }) => {

    const {register, handleSubmit} = useForm()

    const onSubmit = ({ translationText }) => { onTranslate(translationText) }

    return ( 
        <form onSubmit={ handleSubmit(onSubmit) }>
            <fieldset id="translation-fieldset">
                <label htmlFor="translation-text">Translate text to ASL: </label>
                <input type="text" {...register('translationText') } id="translation-input" placeholder="Hello" />
                <button type="submit" id="translation-submit">Translate</button>
            </fieldset>
        </form>

    )
}
export default TranslationForm