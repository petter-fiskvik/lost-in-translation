import TranslationResultLetter from "./TranslationResultLetter"

const TranslationResult = ({ translationText }) => {

    const translationLowerNoWhitespace = translationText
        .replaceAll(" ", "")    // Remove all whitespaces
        .toLowerCase()          // Convert all to lowercase, to match our image files
    
        // Split into an array so we can map through
        const translationArray = translationLowerNoWhitespace.split("")

    const translationList = translationArray.map(
        (letter, index) => <TranslationResultLetter letter={letter} key={index} />
    )

    return (
        <section id="translation-result-section">
            <ul id="translation-list">
                {translationList}
            </ul>
        </section>
    )
}
export default TranslationResult