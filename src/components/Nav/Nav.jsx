import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"

const Nav = () => {

    const { user } = useUser()

    return (
        <nav>
            <section id="nav-section">
                <p id="nav-title">LOST IN TRANSLATION</p>
                {user !== null &&
                    <section id="link-section">
                        <section id="nav-box">
                            <NavLink to="/translation" id="nav-link">Translate</NavLink>
                        </section>
                        <section id="nav-box">
                            <NavLink to="/profile" id="nav-link" >Profile</NavLink>
                        </section>
                    </section>
                }
            </section>
        </nav>
    )
}
export default Nav