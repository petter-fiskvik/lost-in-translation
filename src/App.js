import './App.css';
import {
Routes,
Route,
BrowserRouter
} from 'react-router-dom'
import Startup from './views/Startup';
import Translation from './views/Translation';
import Profile from './views/Profile';
import Nav from './components/Nav/Nav';

function App() {

  return (
    <BrowserRouter>
    <div className="App">
      <Nav />
      <Routes>
        <Route path="/" element={<Startup />}/>
        <Route path="/translation" element={<Translation />} />
        <Route path="/profile" element={<Profile />} />
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
